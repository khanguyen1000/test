let input = {
  id: "#12120",
  currency: "$",
  header: {
    name: "Amir Alaoui",
    points: 1000,
    timestamp: "12-11-2023 2:35PM",
    posName: "POS 1",
  },
  content: [
    {
      title: "Jack",
      value: null,
      items: [
        {
          name: "Waxing 1",
          value: 50.0,
        },
        {
          name: "Manicure 1",
          value: 50.0,
        },
      ],
    },
    {
      title: "Join",
      value: null,
      items: [
        {
          name: "Predicure 1",
          value: 50.0,
        },
        {
          name: "Massage 1",
          beforeValue: 50.0,
          value: 45.0,
        },
      ],
    },
    {
      items: [
        {
          name: "Subtotal",
          value: 200.0,
        },
      ],
    },
    {
      items: [
        {
          name: "Fee",
          value: 200.0,
        },
        {
          name: "Tip",
          value: 200.0,
        },
        {
          name: "Sale Tax",
          value: 200.0,
        },
        {
          name: "Use Tax",
          value: 200.0,
        },
        {
          name: "Disc. By Item",
          value: 200.0,
        },
        {
          name: "Disc. By Ticket",
          value: 200.0,
        },
      ],
    },
    {
      longLine: true,
    },
    {
      title: "Total",
      value: 244.5,
      longLine: true,
    },
    {
      hideLine: true,
      items: [
        {
          name: "Loyalty Program",
          value: 50.0,
        },
        {
          name: "Gift Card",
          value: 20.0,
        },
        {
          name: "Cash",
          value: 100.0,
        },
      ],
    },
    {
      items: [
        {
          name: "Credit Card 6482",
          value: 75.5,
        },
        {
          name: "Auth ID: 97904Z",
        },
        {
          name: "Method: EMV",
        },
      ],
    },
  ],
  signature:
    "iVBORw0KGgoAAAANSUhEUgAAAJUAAAA2CAYAAADOKtsPAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABPYSURBVHgB7Z0HWFPZuoZ3eoEkBAIhSBWDSEeK0gQcvSpi723sogKODbtiRRBHFFEHu6iDeo9dAQUpgpQZEVRQKYGQhN5JQirJXXvuOI9njiNI8ajkffQRyE5Isr/1l+9fO0KQChUqVHztIKB+TkpKCprNZquLxWINbhlXo6ioAG1haztI2MLHt4uFkFTWAakR8RAWg6keZG6et2rVqjpIxSfpd6K6fPkyo7i4WL+5rmEwGoNxFonFA4RCPgaBQKGwWLRcQ1NT0S4UsmUyWQuRSITQWCzU3t4OIZRK1/q6OuK/bt/2gFR8EjT0nXPjxo1Bz54+dRAKRePQGJRlYkJCoxpRnUfV1ipEIBA3ZMoOrsVAa4GdnZ1s3LhxfPAz5d8fY/Xq1bplxcWTdBl6pZCKTvkuI9WhAwfs3rFYPgqpbDRIa5CaOqlEW1vriUQuz2UymY00Gq1t5syZHV15rLi4OO0jh8LvU7U0pWPGjZu8bNmyJkjFJ/kuRKVUKlHR0dFWL3JyxoikUl+lAuqgaWs9Q2Ewd01ptNJasZi/Z88eOfSZgHpLPerY8ctINJLq6eU1LSAgoBFS0SnftKhu3rxplPrkyf+AGmiBRCLDa9I003R0dW8ONR76xme+TxvUA4Cg8FcuXTrV1tpq4+HtPXbNmjX1kIou8c2JqqCgAHvnzp0xr3LzFmJxmAGamrREEkntsa2Dw2uQ0lqhXiAzM5NwJDz8mEQscXTxcJ+7bdu2d5CKLvPNiOr27dsaWRkZczkcXiAKiaw2NRl43HvMqHhvb28x1IvAgjoZFXW+pbHJwtvD3XfDjh1cSMVn8dWL6nZsrHFCcnKggC+YjUajkkd6eoYvXL78FdQHzJgxA4tDY8/x+W1OniO9fdavX18G9YCzZ88aCYXCQU1NTSmgplNA/YSvVlSxsbF6Tx492ssXCkdTSJRLk8dPPe0zzYcH9RHBwcHonMzMMAwaPXHClCmTV6xYUQj1AH9//4VcNjtUKGzvGGxs5Hjy4sUaqJ/wVYkKeEqEtwUFriUlpauUSoWJtrb2kZGjR9+eOHFiO9SHHDp0SPf1i/xdApHQ1NXdfWVQUFA51E0iQ0K033A4gSKBgDncze1MemraOipNa9GJEyf6Tef4VYjqwoULeOBy21ZyeFsVSoW+ri796NTp02+6urqKoF4G2A+I3NxctKOjowz+HhThDDaLtRukVysrO1v/kJCQfKibgGinV1NZvU0mlZLH+vqsfZqaul4kErXpGxoeVqW/LwQ8d0uMi7Oqb2xaLRQILPQN9KMD1679l4GBQa+LCeb58+fES+cuBCqUHdonfvll48GDB43zc3N3CQVCE3tbmzX7wsJeQ90EPNbA8pKSHWKZTOA7YcLugvx8h8qqqiAtEmnJoePH+yxtf438V0QFVjQSjUabVvF4y1tbWj1IZNIdS2vr833pBR0+fNgoOzPzaE119Q+OTk77HZ2df3lw9945gYBPViOTA69fv14MdZNNgYH6DXx+aIe8o26Et1cwh8PRqmCxYnTo9H2Hfv45EepnfPHZ34EDBxgVrPKFQM6eMrnsiauH+2zgVFdAfQRId+ig9esnJD9OOozDY9/6TJgwHoxpav91/fptMDDmjPL03LB2yxYO1E1u3bqldffW7VAMFlM0dqJPRH5+PolXwdlHo+ucJ6irp0D9kC8WqeC66eXLl9Pqa+uW4vG4fFBnHAV1RrdPZleIiIgwzsnIDBa0C0aYm5vvNDQxuVlRUTHsbWHhFQad8cDZdfhGPz+/bjcBSUlJWudOn72JJ+Ayx40fv5vH46m/+D33GB6H4Z29eHEr1E/5IqLat2Pf4DJOyS6lElIz0B0QgSRin3VnFtdVwBCY/ODug1l1tdVrcXjcC0sbmwMSiaRYyOcvLyx8s3fgQJODg4cMOQXSrQTqJqAe1Dh3+sxNPA5bbDN06FpQkGNLi0oiwBvaYetgvwFYCgKon9KnooIL8eTExEUcLm+5BoV8ycLa+iqIDL0ySvkYwJJAlRUXu7wuKNgN0h5G38goxNTUNJlEIlEex8Udbmpqdrd3clwLDo3rSTcGu+5RkVFXCDiM1N3La7FUKsW9yssLA40l1srWesPq1auboX5Mn4kK+DIGebkvImViCcrU3Gzrzp0733xsr1JvER0Rwfjt5cvgpsbmsXoMerjnqFGXCgsL4dTmXvTm7WEkAlnmMMwpaMOGDT0au8Czx/DQsDMQEqHrPGzYNOCliYBpGgIioQZYNFv6u6Bg+kRU6wMD1zQ0NU2jUqkn7Rwc7i5evLhX53MfUlJSgouKiFhexq4I0qXrxNo7OYXBJxY48gYP790LaWvjOw5kDto5f/78u++9qe4CR8LHCQnHGhoatJ2HD18NFomCA1xzhVxR6WZu9vOPQUFCSEXviqoS+ED7Tp8+qOjooDP09ffu3r37bV9Gp+PHj+vlZGbtEQoFLjbW1gG79+9PA78TIWlvn1VUVBxCoVCSre1sd/c0Or3np4A1B9lstpuzy7BZZDIZAdz/MCwOVzqEahnut8evT13/b4leE9WVK1f0H8UlnCeTSMUGJkbbt2zZ0me1E8ymTZucWUXFYTgsrt7ZzQV4pmtrY07F6CRlPNrfxue7mVtY7Bg6dOjDmTNnSqFeYNGCRWvr62sDxviMmygQCORcDieESFBLMjIxutCTgv97pFdEdejAgaEFb95doNI0I93c3C731on8GPAuz3U//TS3nFW+jkKlxIMuLnL79u21u3bt8n6d/zIKjycUOdnbbly/dWuPdhh8yKoVq5aVlZfu8Rw+fKpIJpPWNTbtA41HjIOz882ubkvuT/RYVFuCgqZVVlYd9vDy9O3pZL8zQE2jnvAw7nh9Q8NIRwenFcF7gx8B+wB359atiLqaWh9HJ8dAmULxsDfnbNs2b/7xxYu8yInjfUa0NLeZlXHZm53s7Gb7rVnDglR8lB6J6sDevTOLi4o3jZ80cRJYsZVQH3I0/KhN+rPUU1g0ttHdZZi/P6iTYJE9uP/gmLi9fQAQ9ZLAwMAqqBcJ3R/qnv4s7Zqnt/c8Hoczlt8m8B7n6zMZvNZ+s43liwLmd5Pnzp5dEBIcbA71IXAk8l+1KsjXx6feb/nyHXBLD/8cpEHkwvnzj8+aMTMjJiZGDeplTh07ZjVhvG/F5qCgLQvmzE1eumTJBS6XS4BUdEq3IlVQUJAbWLlnbCzt5m3dtTUP6iNCQ0MtwdjjglKpgNxdXVb9tHFj7vvbQFSyKH737sFwZ9e5zCFMYVtbm4lcLn/UG0Xz0aNHDdPT0h4PMjWt53J5hsCBD91/8OApSMV/AF8g8lt29o96+vr3FyxYUA3/7LMHylu3bvXkstmHDY2Nl3VXUHD08fHxkXzid2jX1dRszc54Nnaw+eD9Y8ePv+Ht7f1vYx0zM7OW8hJWU3Z2ZtTz3N80WltajIFb7gRu6pHIwcDbCtRtsTgMZmBlZXU103TghD0hIX2yfflbAPYBMzIyCFlZWWgwgFevZrNRDq6uNlVcrhHogi2PHz1qJlcosK5ublng8D9E9VmR6o8B7bPMqzp0nePHoqKuf44HBW+OO3jwIK2KW+VVU1M1iUzVOHH+/PmsD4+BxQbGOj5FRSW7ierEEk8vr03AyPzHLg6e8ZWXlxPZLNbQN2/eHLVkMkd2d+8S/PxAZDTNSs+42NjYaGJhZXnK3sHhxPfmkMOvE/yDjY+Px+Xl5RFqa2tx6urqZC0tLcOWxhad2toqtCZVy6y9vZ3U1NyMQCIRVKIakazoUAopFJIaAoXiNzc0vMVgsTxNKpVrZWdXMmjQoGpgLP/l03VZVPAJvxITEwHGaCJLa8ttn5NmwMhGvbSo6Adwsn5iscpGyKRSro2N9Rwwyc9+/0KBaWnKKa8IaW1rsbO0sN6998BeWLSdtuvR0dGYhIcPrxHU1ERz580LfPv2LWbjxo31XRW8MliJDFcL1wH1ks+7wsI1YDHWu41wBxk+6GVfGrd9Dez+4/F4XFlZGammooZE0iAYyDs6rFpaW+EIoyuVSMg4PJ5GUiej5TJpm1QqYYskEi4Ql0wm62DjMKgm8yFD5DQ6nc1gMARgHNViYmLSpclIl0Xlv3Klr6hdHGBtb7t43bp11V29HzjphjnZ2dvB6fkRjG3wLBarVk+XsYfGoEfDrX9YWBgJ1GczeByuP5lCSZs6Y3rYpEmTarv6+H5LljiWssoSnJ2dDpeVlzsKBQJbfQODdb+cOfPgU/eDhbxu9ToTNBbpW1tXO4VdwfZAo9G5biM8Zu/fv7/8/TGpqak48KX87+n3awIe3PP5fGx2djaFRqMZyMQyy5pq3iCBUKgPhKWLQmE6RCJRTbtI+I5K1eLRqJQKOyenRktLy2owcO91k7pLogJpj/Es7elNHA67++r164+7ch+43U9+nBRQU1uzCYRGan19fQeHy8kBTvemkJCQTHCyUE9TUsbk578KxuEwbQOZzI2fuz8cvgImLzf3nFwmmyaVykSg/vqlsbEZ+JOSMdqatAIECsEGs8fTH+6MAM9LNzM9/YfWVv5cqUxCIOAIT+kMeturvJebTUyMpkSeOpV57949Ynpq6kjwnOcoIYQOHocVg3rilou7+9W+NHa78HqRtra21IqKCp0OaYc+BoM0kYjFA8QSiaZMJscqlIoaPJHYxOFw8sECrrO3t2+dPXt29ZeOuF0q1EtLS+eAJ9wwYuTIDCCqTo8P2bt32I1fYyPRGIyTsbEx4tWr11wNKuWkm4fHWZCaGjSJRJPXb4u2CQR8b3Mz5oF5ixZdtbKy+uyTZWRkxEhOSnIj4PA8z5HeK3fu3JkK0pbvu8I3k9qEfElrc8tCIV+oDVbytqzkdJeKqool927fsSOqq70D7n+0u7v7s6lTpzYumD//LIlKuffj0qW/0xgMt2tXfj2AxKDQugzGOU01tZxWgUCzuqY2LP7hQwsQvbZ0JS33hOfPn1NAYazZ3tqqBWoYTXUSyQLeBQEcXRoYYCOQCAQPT1ZrqKurK8JgMKmuHh6to0aN+mh0nzNnDvSl6TRShYeHq+VkZsaDMHr2SmxszKeOBVEAm5qcvOL3nJwQLBZHALm4WEODcnWYg0PssoAA9uXLl4lZGRkz6+rqA8hk0u8jR4/e874N7Q5wNMxMS585cDDz/vv97eCEYEB9pDZlypQWkF5/SE9JvczQG5AmFolstOnal5mDB19raWnhfOi6z5s1K0WJQj3BItFYkbh9sqGh4RGfCROufXj1czSwGeKTk+PBIok4Ghl5Fuomf6bUAeAvGhQvZFAQa9JpNDpIvQNkcjlFoVCQwTEdKBRKSAJRR4lGN7Q1Npbom5g06ujo1IOuuUefEfEl6FRU/v7+Rq9e5N3XoussvnPnTu7fb4ffpFu3bg1ISUy0rG1oWNrc0OhI16Wn6RkYxLpaWPw2ZfHiFjjnJ8XHO7IqOFs6pFKavoHRz6PHjo77lK3QG4CxEaatqfkqUU29epj18H1+G/0aPnbc0qVLNzXW10/S19N7Ym1vHwnS5UePOxwa6p6ZmX0EdEEpYrk8ha5FF6tR1CDgj0EgkkCgAIYELS3w91g9oEwiHs8A/hlKIpKgQGQ0QCORRC6PV29oZKQtEorK9AYwEFKplIXFYJqb6utLLS3s+eoo9QamE7O9O5H7a6FTUW3atIlUUlSUQCFR0hcuXRwFugB5ZWUl5nl2tjm7vMIai8e6gNRg39TQoM/Q0zvn4u52BBiTZe/zOOj8BmVnZCxVKqHhOBw+fri97aXla9d2uRDvKcBywIPoIoX3Pv3TMXCE1dTUJKWnpzd3NjeETdemhqYpBCLeHI/FYdAYtBikIBkohJsU/w8EgT8g8khkcmkNuK1ZQ0ND1NTayhk6dKgcRJuaIUOGSJlM5lcfcbpLlwr1qKgos9+ysoIhCKmDQEJy+I0D9VItXUenqLWl1QukGwtLc4uAsRPHP3zfJf1RqD9KnFff1BBAJBASQWt6kkwml30vF1X+mcZQXl5eHd+y9dAXdNlS+NNLooDOAwIdkILNZtMyUlKiOpQQ1nGY8/IPLxXfvnmzV0kJaxt4p1G6AxhhYFWmqfYc9R+6PKb5czW2wF+Drmvsq/z841oa1KThHu7b339k4cmTJ6kvc3ODSkpZvjraWjHGTOZFuNuDVPQrUJ9zMDw8VCMQtpaWlOw0HThw1/TZs8KnTZsmhKOYTCieDUzOiwgkGmlsarKSRKE82L59u2rPdj+ky+nvwoULuo/i4mIwOJzMwdFxydo/i+0jhw55PM3IOAGKVpGX5wj/lYGBzyEVKjoDeFUu06dOLVi13C80MjISHlvAV+fSly1ZcmrqpMlVwTt3LoG3+UIqVECdpD94I5xUJJqfn58fBdryECpNK8LX1xeJx2CWJCQkxAJ/hes96oeZG4OC0kFXp+qAVPzBP6Y/eP71OC4hiFfJGwW6uw3Ab3nLKipykUhlG1uamxUeHu77N2ze/EzVTqv4Ox8V1a+//kpLjE+IBmMDgZuzU3BpeblWa1vbKjBSMDEwNIycwWTGOfr59ejCTBXfL/8hKvijfl7nv7yGIxBYZiZmkRweeyIYNYwB4/yEIdbW0cCPUv2HPyo+yb+JChTkJkmPE/+XoUtn6+rqPgYG53QcHs+ysrE5AcRUAKlQ0QX+Mj9BV6f/8P792xx2hTUBj9eFkEiSlYXtfjNLs2eqCyZVfA5/iSo35/cN7Xwhwcl52AmmudkJMGkv2bFnR7/58FMVfQBcS4EhsCakQoUKFSpUfPf8H4nVMy1BqddFAAAAAElFTkSuQmCC",
  // vouchers: [
  //   {
  //     code: "1039402140",
  //     barcode: "base64",
  //     amount: 5,
  //     currency: "$",
  //     descriptions: [
  //       "For your next visit only",
  //       "Valid from Monday - Thursday",
  //       "Can not combine with any other offers",
  //     ],
  //     expired: "08/30/24",
  //   },
  //   {
  //     code: "1039402140",
  //     barcode: "base64",
  //     amount: 5,
  //     currency: "$",
  //     descriptions: [
  //       "For your next visit only",
  //       "Valid from Monday - Thursday",
  //       "Can not combine with any other offers",
  //     ],
  //     expired: "08/30/24",
  //   },
  //   {
  //     code: "1039402140",
  //     barcode: "base64",
  //     amount: 5,
  //     currency: "$",
  //     descriptions: [
  //       "For your next visit only",
  //       "Valid from Monday - Thursday",
  //       "Can not combine with any other offers",
  //     ],
  //     expired: "08/30/24",
  //   },
  //   {
  //     code: "1039402140",
  //     barcode: "base64",
  //     amount: 5,
  //     currency: "$",
  //     descriptions: [
  //       "For your next visit only",
  //       "Valid from Monday - Thursday",
  //       "Can not combine with any other offers",
  //     ],
  //     expired: "08/30/24",
  //   },
  // ],
};
