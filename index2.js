var exportData = () => {
  showBill();
};
var showBill = () => {
  var printWindow = window.open("bill2.html", "bill2", null);
  let input = {
    id: "#12120",
    currency: "$",
    header: {
      name: "Amir Alaoui",
      points: 1000,
      timestamp: "12-11-2023 2:35PM",
      posName: "POS 1",
    },
    content: [
      {
        title: "Jack",
        value: null,
        items: [
          {
            name: "Waxing 1",
            value: 50.0,
          },
          {
            name: "Manicure 1",
            value: 50.0,
          },
        ],
      },
      {
        title: "Join",
        value: null,
        items: [
          {
            name: "Predicure 1",
            value: 50.0,
          },
          {
            name: "Massage 1",
            beforeValue: 50.0,
            value: 45.0,
          },
        ],
      },
      {
        items: [
          {
            name: "Subtotal",
            value: 200.0,
          },
        ],
      },
      {
        items: [
          {
            name: "Fee",
            value: 200.0,
          },
          {
            name: "Tip",
            value: 200.0,
          },
          {
            name: "Sale Tax",
            value: 200.0,
          },
          {
            name: "Use Tax",
            value: 200.0,
          },
          {
            name: "Disc. By Item",
            value: 200.0,
          },
          {
            name: "Disc. By Ticket",
            value: 200.0,
          },
        ],
      },
      {
        longLine: true,
      },
      {
        title: "Total",
        value: 244.5,
        longLine: true,
      },
      {
        hideLine: true,
        items: [
          {
            name: "Loyalty Program",
            value: 50.0,
          },
          {
            name: "Gift Card",
            value: 20.0,
          },
          {
            name: "Cash",
            value: 100.0,
          },
        ],
      },
      {
        items: [
          {
            name: "Credit Card 6482",
            value: 75.5,
          },
          {
            name: "Auth ID: 97904Z",
          },
          {
            name: "Method: EMV",
          },
        ],
      },
    ],
    signature:"iVBORw0KGgoAAAANSUhEUgAAAb8AAAD2CAYAAABGM4liAAAAAXNSR0IArs4c6QAAF35JREFUeF7t3T8SNEd5B+ChioAMhWRIJ7A5wQeRQ46AOIHRCQQngBtYnAAIHYFOgH0CmYwMhQ6owvVS29BuzezOzE7PdE8/W0UJfd9uT/fz9u5PPX+/NXkRIECAAIHBBL412HgNlwABAgQITMLPJCBAgACB4QSE33AlN2ACBAgQEH7mAAECBAgMJyD8hiu5ARMgQICA8DMHCBAgQGA4AeE3XMkNmAABAgSEnzlAgAABAsMJCL/hSm7ABAgQICD8zAECBAgQGE5A+A1XcgMmQIAAAeFnDhAgQIDAcALCb7iSGzABAgQICD9zgAABAgSGExB+w5XcgAkQIEBA+JkDBAgQIDCcgPAbruQGTIAAAQLCzxwgQIAAgeEEhN9wJTdgAgQIEBB+5gABAgQIDCcg/IYruQETIECAgPAzBwgQIEBgOAHhN1zJDZgAAQIEhJ85QIAAAQLDCQi/4UpuwAQIECAg/MwBAgQIEBhOQPgNV3IDJkCAAAHhZw4QIECAwHACwm+4khswAQIECAg/c4AAAQIEhhMQfsOV3IAJECBAQPiZAwQIECAwnIDwG67kBkyAAAECws8cINC/wA8fQ0j/jH/9ef/DMgIC9QSEXz1bLROoKRBB9/lM6OXb/NE0TX+o2QltE+hVQPj1Wjn9HkUgVnAfpmnKV3Vbxu47vkXLe4cR8MUYptQG2plAhF5a2b3Tdau/d/R89rYCwu+2pTWwjgV+v2Gll3Zr/iIbb3w+vYRfxxNB1+sJCL96tlomsEdgacWXH7v78nEsb+54Xv75+PsIPy8CBAoB4WdKEGhHYC74YkW35czNv1n1tVNQPWlXQPi1Wxs9G08gVmpxckt6bQ2+OCkm7fK06htv/hjxBgHhtwHLWwlUFshXbbGprd/Pr6Zp+vjRR8f6KhdL830LbP1y9T1avSfQrkC+aku93PL9dKyv3drqWYMCW75cDXZflwjcRqA83rdlt2UZnFZ9t5kWBlJLQPjVktUugW0C74RffmnEltDc1kPvJnAjAeF3o2IaStcCe8Ov/JzvdNfTQOfPEvBFOUvadgg8F9gTfnZ3mlUEdgoIv51wPkbgYIE9J7zkuzu3XhZxcPc1R6AvAeHXV7309t4CWy51+Nk0Tb98cDjOd+95YXQVBIRfBVRNEtgpUN7T89n3051cdiL7GIEQEH7mAYF2BNaGX3580O7OduqnJx0JCL+OiqWrtxcow2/uej3Bd/tpYIBnCAi/M5Rtg8A6gfKMz3h6Q/kQ23x3p+/vOlfvIvANAV8ek4JAWwLPTnqx6murVnrTsYDw67h4un5LgS+mafpJNrJ0TE/w3bLcBnWVgPC7St52CcwLzD3TL4795U9n9701ewi8KeBL9CagjxOoIFAG4P9kjypydmcFcE2OJyD8xqu5EbcvMLf6i14LvvZrp4edCAi/Tgqlm8MJzK3+PhlOwYAJVBIQfpVgb9xsnHofP8zfnabps2ma4tZaXscLzN3r89fTNH16/Ka0SGA8AeE3Xs3fHfFfpmn6KGvEHHpX9Jufnwu+9C4Pqj3eW4sDCvjhGrDobw65vA7Ncag3QWc+nhv/eZqm7xXvYX68uRYHExB+gxX8gOEKvwMQnzSR+6aQK297Fh8XgHXroPWbCwi/mxe4wvDK8ItNmEfHQOchlz+mKHaDfj5zqzO7QI9x18qAAn60Biz6m0OeCz+rkDdRHxex5/fxzIMt/jwFYL4l4fe+uxYGFRB+gxb+jWELvzfwFj5aXtaQQm3per/UjPA7vhZaHERA+A1S6IOGuXQWYtyBxDVo+5DL43mxio5dnvntzOZattre5+1TBP4uIPxMhC0C5U2X88/ecS7Fyuvrx6Ud8f+Pfs09v29u92a5XcF3XCXSruY4phqveIxUjVof12MtHSJwxx+sQ2A0Miswd9bhHXfBLZ1gcmTolJZxAfv3Z05qyQsR24+XH+d9X9AUdPHPeHJG7LEon5cYLdudvM+3q08Jv67KdXln0w927JYrfzSODIarB/os5I/4cQy/D9kgY7WR/7vAO24G5CvpuaCb25LfxeP8m21JkZstTXMdy4/3pVua5T8mdwm/V8GXCrP3u1OexBIrvvz5fan9/FKH5iZDwx1Kc/KX0zT968p+pvkc/xES/98t+1bC9fy2vV/gnses7/sE8vCL3ULlsam7hN/c2axzK7M9u8bK4PuvhR/ou1jum2nbP7V1dRfh9rtpmsJf0G33vsUnhN8tynjKIPIf7pg35Q/5HVYqc2ezLl12sPUM13JFmT+jr9zN6Zje6ymdrn2M3cWvdmfG3EzHS4Xda9sh3iH8hijzIYPMj/dFIIwSfvl35I/FSm3t96e0+t9pmr5TVMVq758gebCVk/fj7MG+zyZ27E6Os5OF3SFf//s1svbLe7+RG9FWgbQ7MP1Il6uku6788u9IvnqLXWY/WIH46kL1aGLk4MvPwFyzintGnlZ4Am/FxBz9LcJv9Bmwbvzl8b50tmd+IfZdwy/t9izD/lVgLV0ukYuP8Hy+tIqLFdvciT3rZuDzdwm9IxQHa0P4DVbwncPNf/jzOZOfHHLX8EvjKo/ZPfvujLzai7kSD9yNsFu6fGPnNPzHx+J46U8f/2aV967moJ8XfoMWfuOwy+N96eN7wi+tiP70aCR+yFo6wWPubM//nKbp3zKzuTM9Y1xrT6/fc6boxpKd9vatZ1pu7Vgebk5a2arn/YsCws/kWCOQAqFc3W0Jv1e7AfNrra4Mw6X7l+ZO0dfy8odXZxze5dje3uvowitWg/EfO/GKVeFHj/+lP0vX2cXfW9Gt+WZ6z24B4bebbpgPzh3vS4PPz358dgxszW7AHPTqldHaC923TIJXxwi3tHX2e7eu7tJ1dBFuV/6HzNlOtteRgPDrqFgXdTUPgjKUvspOO186eWNr8MUwrw6/6MOefs+VqNfQ2xJ4rqO76Mtps/sFhN9+u1E+uXS8rwyIpR/5uWNoya71sx0jAP/9sWvuVb0jAGKlk99Sq8fge7V7Ou2SdIeUVzPC3zctIPyaLs/lnSvv5xkrsvyVr47mzvbMV4b553o5Nb08/vdZEYRx3Codp4pH4uTH/eLP1xwHvLzIjw48C71e6tWKpX50ICD8OijShV18dryvXPmV4be027CFXZprSfNdvs8u5ShXtz2NcSn0BN7aWeJ9XQoIvy7Ldlqn8x//ublSroyWrgFMHe4tFPKL+Jf6XoZ8D2NMK9JytZp2aaanyZ820WyIwNkCwu9s8b629+x4X4xkKfzmzpbs7fhXrHzSRdpLq75ynK2P0a7Nvr5/eltRQPhVxL1B00eFX493f8l3ZZaruQiRWPHldzBpMfierfDS9EyrPNfV3eALawjrBYTfeqsR35kCYGlX3tzKb+4i8R52Beb1fXYD67ljmS0FX7qX5rObRDueN+K32Zj/n4DwMyGWBF6d7LK027P3Rx2V4Z3f2Dq/9i25xRmgv7pwGq0JO8fyLiyQTbcpIPzarEsLvcpD4Nk8yXcPxvvyY2XpR7e8RKKF8S31Ye4Mz6Vbnl2x4luzKzONzQqv5Zmmb5cKCL9L+ZveePnk9qXOluHX+2n/5RmeMe78z+Lfzwq9FHTxz3R88dm1g+meo/FPx/Ca/nrp3NUCwu/qCrS7/VeXOaSelyeGlEHRyxwrz4RMJ4KU43n34vUUXumZiPkMiL+LZ97FjZ7XXCCfAs6lCe1+j/SsUYFefpga5bt1t16d6TkXfvF08/z2Xj2d5flF8bDV2FW7tOLLV2ThkJ/1mYdWjD89yWBNmK2ZUO6juUbJewi8EBB+psir3ZmvAqw8MzIPv17O8pw7yeU/spt2h1GsxiLIznzlK7vYrl2ZZ+rb1q0FhN+ty/vW4Jae4Vc2+uzxP73Mr7mTXJ7dkPst2JkPp2N1KeCE3NHC2iNQCPTy46Rw5wvsCb8/T9P0vayrPcyvpUsbjgi/CLF4Yn3sUk0vwXb+XLZFAt8Q6OHHSdnOF8gD4dWZjUu7PV/tLj1/VPNbXHoa/dLlDXOt5IEWXmkF18oY9YMAASs/c2CFwN7wy5vuIfzKXbZLN+8OjzzgrN5WTCJvIdCygJVfy9W5tm9pRfRq5bf06KLWw29pd+e16rZOgMApAsLvFObuNrJl5ddj+JXX9LUe1N1NIB0m0LqA8Gu9Qtf074jwi0sDPrmm+y+3Wt6CzffgJZk3ELiXgC/9vep55GhePdEhbSsPyvxsz19P0/TpkR06qK0eHz570NA1Q4BAEhB+5sKcwJonOuSfyy+LyG/f1eINrfOzO1sNaLOSAIHKAsKvMnCnzW8Jv/y9+e3NWjyOVq76zP9OJ6huE3hXwJf/XcF7fv6O4VcG36uzWO9ZWaMiQODvAsLPRJgTyIPi1f05l475tXTCy9wF67XnfjqjNJ4CEZ5eBAg0JFD7B6ChoerKBoEt4RfNpuNoX0/T9NFjO7EL9AcbtlnrrXPBd/SqL3/AbIyjfILDq/+AqDV27RIgsCAg/EyNd1d+efjlTz5o4ZhfeT1f9PXd4HsVdHOews/3jEBjAsKvsYI00p29K7+8+1eHX4whHgybP4ZoT/BF2EVbsbpd+0w+T1RvZCLrBoElAeFnbhy58svbunK1M3fXmS3BN7difDZTPHfP94hAZwLCr7OCndTd/IbPa0Jj7pl+V4TfUmituZ5vT+B5gsNJE9JmCBwtIPyOFr1He/nKaW/4rfncu1ppN2T097vTNOVPkU9tP+vH1sBLxwxjpefJDu9Wz+cJXCgg/C7Eb3jTW8Pv2YNf0+qoHG4KjzUhEiGVgu7HCyE3x7kUfHtCb83qseGS6hoBArmA8DMfXh3zW7OCS7s9/zpN07cPIs3PHN3a5BGhlx/HWxPQW/vo/QQIXCgg/C7Eb3jTW1d+KfzSWY6fXzC2uJj8t9M0/Wpm21tWejGGCE+Bd0ERbZLAWQLC7yzpvrbzTvjFiS7pjiY1QjBC6XePi+nX7Dpdet5gXhGB19f81FsCbwsIv7cJb9nAu+GXo+TH69KfR9jEn8c1eN9fEPzTNE1fFH+3ZTUWY/jw5No8gXfLqWtQBNYJCL91TqO968jwO9suXZQewVe+BN7Z1bA9Ao0KCL9GC3Nxt7Y8yT26moflVXNq6biewLt4Mtk8gRYFrvqhatFCn/4pEIGRVk5rTvG/Mvzmdm/GTbU/c9KKKU2AwJKA8DM35gTyO7asuUfnFeE3dyKLVZ75TIDAKgHht4ppuDdtDb98N2ntOSX0hpuOBkzgeIHaP1TH91iLZwi0GH5zuzet9M6YDbZB4IYCwu+GRT1gSOWNql/Nk5orv7kTWYTeAUXWBIGRBV79qI1sM/LYy3t1vponNcJP6I08A42dQGWBVz9qlTev+UYFtoZfDCM+s+bkmFdDFnqvhPw9AQJvCwi/twlv2cDe8AuMvXNK6N1yKhkUgTYF9v5QtTkavTpK4Mzwi9D7dJqmn2Sdd0zvqEpqhwCBWQHhZ2LMCZwRfnM3vxZ65iMBAqcICL9TmLvbyDvhF091eHYD6rnQS48Q2nLj6u5QdZgAgXYEhF87tWipJ189nrgQfYqHyn6yonMpMJfCb+7i9DUPyl2xaW8hQIDANgHht81rlHfnK7+1Z3CmawPLOSX0Rpk1xkmgIwHh11GxTuzqO+EXK794pbM3825b6Z1YRJsiQGBZQPiZHaVAfsF6/N3awEorv/+epulfikbXtqEaBAgQOEVA+J3C3NVG9oRffOY30zR9lI00dpd++XjWX1cAOkuAwP0FhN/9a7x1hGX4PTt7c+7C9DhB5qeepbeV3fsJEDhTQPidqd3HtsoTVObmyFzoff1Y+dnF2Ued9ZLA0ALCb+jyzw7+WfjF38WdWD4udm9G4H3+OMlF+JlTBAg0LyD8mi/R6R0swy92e8ZK78Pjn6lD5TG9dMKL8Du9ZDZIgMBWAeG3Vez+7y/DL47h5Su9EJg7Dpg+J/zuP0eMkED3AsKv+xJWGUB5e7O0kWfBJvyqlEKjBAjUEBB+NVT7b3Purixr7tkZx/3W3hGmfyUjIECgWwHh123pqnf8i8fJLWuftJAukRB+1UtjAwQIvCsg/N4V9PkkIPzMBQIEuhEQft2UqvmOCr/mS6SDBAgkAeFnLhwlkN8Zxrw6SlU7BAhUEfAjVYV1yEaF35BlN2gCfQoIvz7r1mKvhV+LVdEnAgRmBYSfiXGkQLo+0Lw6UlVbBAgcLuBH6nDSoRsUfkOX3+AJ9CMg/PqpVQ89TeH36oL4HsaijwQI3FhA+N24uBcMTfhdgG6TBAhsFxB+2818YlkgPdnBys8sIUCgaQHh13R5uuuc8OuuZDpMYEwB4Tdm3WuNOt0Q28qvlrB2CRA4RED4HcKokYeAB9qaCgQIdCEg/LooUzedFH7dlEpHCYwtIPzGrv/Ro/dA26NFtUeAQBUB4VeFddhGhd+wpTdwAn0JCL++6tV6b9P9PX8xTVMEoRcBAgSaFBB+TZal204Jv25Lp+MExhIQfmPVu/ZohV9tYe0TIHCIgPA7hFEjDwHhZyoQINCFgPDrokzddFL4dVMqHSUwtoDwG7v+NUYfN7d2wksNWW0SIHCYgPA7jFJDD4EIvz9M0xS3OPMiQIBAkwLCr8mydN0p4dd1+XSewBgCwm+MOp85Srs9z9S2LQIEdgkIv11sPvREIO7v+aWL3M0RAgRaFhB+LVenz75F+MXLMb8+66fXBIYQEH5DlPnUQTrmdyq3jREgsEdA+O1R85lnAnZ7mh8ECDQvIPyaL1F3HbTbs7uS6TCB8QSE33g1rz3ieJrDB8f8ajNrnwCBdwSE3zt6PjsnIPzMCwIEmhcQfs2XqLsOCr/uSqbDBMYTEH7j1bz2iB3zqy2sfQIE3hYQfm8TaqAQEH6mBAECzQsIv+ZL1F0HI/zi0UbmVnel02EC4wj4gRqn1meNVPidJW07BAjsFhB+u+l8cEHAbk9TgwCB5gWEX/Ml6q6Dwq+7kukwgfEEhN94Na894ri3Z7zMrdrS2idAYLeAH6jddD64ICD8TA0CBJoXEH7Nl6i7DsaZnvG/uNjdiwABAk0KCL8my6JTBAgQIFBTQPjV1NU2AQIECDQpIPyaLItOESBAgEBNAeFXU1fbBAgQINCkgPBrsiw6RYAAAQI1BYRfTV1tEyBAgECTAsKvybLoFAECBAjUFBB+NXW1TYAAAQJNCgi/JsuiUwQIECBQU0D41dTVNgECBAg0KSD8miyLThEgQIBATQHhV1NX2wQIECDQpIDwa7IsOkWAAAECNQWEX01dbRMgQIBAkwLCr8my6BQBAgQI1BQQfjV1tU2AAAECTQoIvybLolMECBAgUFNA+NXU1TYBAgQINCkg/Josi04RIECAQE0B4VdTV9sECBAg0KSA8GuyLDpFgAABAjUFhF9NXW0TIECAQJMCwq/JsugUAQIECNQUEH41dbVNgAABAk0KCL8my6JTBAgQIFBTQPjV1NU2AQIECDQpIPyaLItOESBAgEBNAeFXU1fbBAgQINCkgPBrsiw6RYAAAQI1BYRfTV1tEyBAgECTAsKvybLoFAECBAjUFBB+NXW1TYAAAQJNCgi/JsuiUwQIECBQU0D41dTVNgECBAg0KSD8miyLThEgQIBATQHhV1NX2wQIECDQpIDwa7IsOkWAAAECNQWEX01dbRMgQIBAkwLCr8my6BQBAgQI1BQQfjV1tU2AAAECTQoIvybLolMECBAgUFNA+NXU1TYBAgQINCkg/Josi04RIECAQE0B4VdTV9sECBAg0KSA8GuyLDpFgAABAjUFhF9NXW0TIECAQJMCwq/JsugUAQIECNQUEH41dbVNgAABAk0KCL8my6JTBAgQIFBTQPjV1NU2AQIECDQpIPyaLItOESBAgEBNAeFXU1fbBAgQINCkgPBrsiw6RYAAAQI1BYRfTV1tEyBAgECTAsKvybLoFAECBAjUFBB+NXW1TYAAAQJNCgi/JsuiUwQIECBQU0D41dTVNgECBAg0KSD8miyLThEgQIBATQHhV1NX2wQIECDQpMD/AXDWSyTmxfvUAAAAAElFTkSuQmCC",
      
    vouchers: [
      {
        code: "1039402140",
        barcode: "base64",
        amount: 5,
        currency: "$",
        descriptions: [
          "For your next visit only",
          "Valid from Monday - Thursday",
          "Can not combine with any other offers",
        ],
        expired: "08/30/24",
      },
      {
        code: "1039402140",
        barcode: "base64",
        amount: 5,
        currency: "$",
        descriptions: [
          "For your next visit only",
          "Valid from Monday - Thursday",
          "Can not combine with any other offers",
        ],
        expired: "08/30/24",
      },
      {
        code: "1039402140",
        barcode: "base64",
        amount: 5,
        currency: "$",
        descriptions: [
          "For your next visit only",
          "Valid from Monday - Thursday",
          "Can not combine with any other offers",
        ],
        expired: "08/30/24",
      },
      {
        code: "1039402140",
        barcode: "base64",
        amount: 5,
        currency: "$",
        descriptions: [
          "For your next visit only",
          "Valid from Monday - Thursday",
          "Can not combine with any other offers",
        ],
        expired: "08/30/24",
      },
    ],
  };
  setTimeout(() => {
    printWindow.postMessage(input);
  }, 100);
};
