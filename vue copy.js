var head = `<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="style.css">
<link href='https://fonts.googleapis.com/css?family=Open Sans' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>`;
var body = `<div id="content-to-print">
    <div id="header" class="margin-25">
        <div id="id" class="justify-content-left">
            <h1>{{id}}</h1>
        </div>
        <div class="justify-content-space-between">
            <span style="font-size: 20px; font-weight: bold">{{header.name}}</span>
            <span class="gray-text-color">{{header.points}}&nbsp;Points</span>
        </div>
        <div class="justify-content-left gray-text-color">{{header.timestamp}}</div>
        <div class="justify-content-left gray-text-color">{{header.posName}}</div>
    </div>
    <div id="content">
        <div class="margin-25">
            <div id="cart">
                <ul v-for="group in content.cart.data">
                    <br>
                    <div id="group">
                        <div style="font-size: 18px; font-weight: bold;">{{group.groupName}}</div>
                        <hr class="margin-0">
                        <ul v-for="item in group.items">
                            <div id="items">
                                <div class="justify-content-space-between">
                                    <span>{{item.name}}</span>
                                    <span>
                                        <span class="text-line-through" v-if="item.beforePrice != null">{{currency}}{{item.beforePrice.toFixed(2)}}</span>&nbsp;
                                        <span>{{currency}}{{item.price.toFixed(2)}}</span>
                                    </span>
                                </div>
                            </div>
                        </ul>
                    </div>
                </ul>
                <hr>
                <div class="justify-content-space-between">
                    <span>Subtotal</span>
                    <span>{{currency}}{{content.cart.subTotal.toFixed(2)}}</span>
                </div>
                <hr>
            </div>
            <div id="fees-discounts">
                <div class="justify-content-space-between">
                    <span style="font-size: 14px;">Fee</span>
                    <span>{{currency}}{{content.fees.fee.toFixed(2)}}</span>
                </div>
                <div class="justify-content-space-between">
                    <span style="font-size: 14px;">Tip</span>
                    <span>{{content.fees.tip ? currency + content.fees.tip.toFixed(2) : '--'}}</span>
                </div>
                <div class="justify-content-space-between">
                    <span style="font-size: 14px;">Sale Tax</span>
                    <span>{{currency}}{{content.fees.saleTax.toFixed(2)}}</span>
                </div>
                <div class="justify-content-space-between">
                    <span style="font-size: 14px;">Use Tax</span>
                    <span>{{currency}}{{content.fees.useTax.toFixed(2)}}</span>
                </div>
                <div class="justify-content-space-between">
                    <span style="font-size: 14px;">Disc. By Item</span>
                    <span>{{currency}}{{content.discounts.discountByItem.toFixed(2)}}</span>
                </div>
                <div class="justify-content-space-between">
                    <span style="font-size: 14px;">Disc. By Ticket</span>
                    <span>{{currency}}{{content.discounts.discountByTicket.toFixed(2)}}</span>
                </div>
            </div>
        </div>

        <hr>
        <div class="justify-content-space-between margin-25">
            <span style="font-weight: bold">Total</span>
            <span style="font-size: 20px; font-weight: bold;">{{content.total}}</span>
        </div>
        <hr style="border: 1px dotted gray">
        <div id="excepted" class="margin-25">
            <div class="justify-content-space-between">
                <span>Loyalty Program</span>
                <span>{{currency}}{{content.excepted.loyaltyProgram.toFixed(2)}}</span>
            </div>
            <div class="justify-content-space-between">
                <span>Gift Card</span>
                <span>{{currency}}{{content.excepted.giftCard.toFixed(2)}}</span>
            </div>
            <div class="justify-content-space-between">
                <span>Cash</span>
                <span>{{currency}}{{content.excepted.cash.toFixed(2)}}</span>
            </div>
            <hr class="border-dotted-gray">
        </div>
        <div id="payment" class="margin-25">
            <div class="justify-content-space-between">
                <div>
                    <span>Credit Card</span>
                    <span>{{content.payment.creditCardNumber}}</span>
                </div>
                <span>{{currency}}{{content.payment.pay.toFixed(2)}}</span>
            </div>
            <div class="justify-content-left">
                <span>Auth ID:&nbsp</span>
                <span>{{content.payment.authId}}</span>
            </div>
            <div class="justify-content-left">
                <span>Method:&nbsp</span>
                <span>{{content.payment.method}}</span>
            </div>
        </div>
        <div class="margin-25">
            <div class="gray-text-color">Signature</div>
            <div style="text-align: center">
                <img id="signature" :src="\`data:image/png;base64,\${signature}\`" />
            </div>
        </div>
        <hr class="margin-25">
        <p style="text-align: center">Thank you for using our service</p>
    </div>
</div>
<script src="index.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.js"></script>
`
function showBill() {
  var win = window.open(
    "index-will-show.html",
    "",
    "left = 0, top = 0, toolbar = 0, scrollbars = 0, status = 0"
  );

  var content = "<html>";
  content += head;
  content += '<body>';
  content += body;
  content += "</body>";
  content += "</html>";
  win.document.write(content);
  win.document.close();
}

var exportData = () => {
  showBill();
}